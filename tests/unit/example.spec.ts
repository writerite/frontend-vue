import { shallowMount } from '@vue/test-utils';
import WrFooter from '@/components/WrFooter.vue';

describe('WrFooter.vue', () => {
  it('renders', () => {
    const wrapper = shallowMount(WrFooter, {
      propsData: {},
    });
    expect(wrapper.text()).toMatch('Built');
  });
});
