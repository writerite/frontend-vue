import Buefy from 'buefy';
import VeeValidate from 'vee-validate';
import Vue from 'vue';

import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import { createProvider } from './vue-apollo';

Vue.config.productionTip = false;

Vue.use(Buefy);
Vue.use(VeeValidate);

export default new Vue({
  router,
  store,
  // https://github.com/apollographql/apollo-link/issues/773
  apolloProvider: createProvider({
    httpLinkOptions: {
      credentials: 'same-origin',
    },
    // getAuth,
  }),
  render: (h: any) => h(App),
}).$mount('#app');
