import { DollarApollo } from 'vue-apollo/types/vue-apollo';
import { ComponentOptions } from 'vue';
import { Vue } from 'vue-property-decorator';

declare module 'vue/types/vue' {
  interface Vue {
    $apollo: DollarApollo<any>;
    errors: any;
  }
}
